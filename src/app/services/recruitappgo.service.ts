import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RecruitappgoService {
  RecruitApp_Go = environment.go_url;
  RecruitApp_Node = environment.node_mongo_url;


  constructor(private https: HttpClient) { }

  register(d) {
    console.log('User Registration:', d);
    return this.https.post(this.RecruitApp_Go + '/user/register', d)
      .pipe(map((response: any) => response));
  }

  login(d) {
    console.log('Login details', d);
    return this.https.post(this.RecruitApp_Go + '/auth/login', d)
      .pipe(map((response: any) => response));
  }

  getAllJobPosts() {
    return this.https.get(this.RecruitApp_Go + '/jobPosts')
      .pipe(map((response: any) => response));
  }

  getAllJobPostDescriptions() {
    return this.https.get(this.RecruitApp_Node + '/job-descriptions/')
      .pipe(map((response: any) => response));
  }

  // get all job posts by user
  getUserCreatedJobPosts(d) {
    return this.https.get(this.RecruitApp_Go + '/job-descriptions/job-descriptions/' + d)
      .pipe(map((response: any) => response));
  }
  
  goGetUserCreatedJobPosts(d) {
    return this.https.get(this.RecruitApp_Go + '/jobPosts/' + d)
      .pipe(map((response: any) => response));
  }

  getUserMenuType(userType: string): Observable<any> {
    return new Observable((observe) => {
      console.log(sessionStorage.getItem(userType));
      observe.next(sessionStorage.getItem(userType));
    });
  }

  createUserProfile(d) {
    console.log(d);
    return this.https.post(this.RecruitApp_Node + '/users/add_profile', d)
      .pipe(map((response: any) => response));
  }

  createJobPost(d) {
    console.log(d);
    return this.https.post(this.RecruitApp_Go + '/jobPosts/create', d)
      .pipe(map((response: any) => response));
  }

  saveJobDescription(d) {
    console.log(d);
    return this.https.post(this.RecruitApp_Node + '/job-descriptions/add_jobDescription', d)
      .pipe(map((response: any) => response));
  }

  go_mongo_interface(d) {
    console.log(d);
    console.log(d.documentID);
    return this.https.post(this.RecruitApp_Go + '/receiver', d)
      .pipe(map((response: any) => response));
  }

  initApplication(d) {
    console.log(d);
    return this.https.post(this.RecruitApp_Go + '/application/create', d)
      .pipe(map((response: any) => response));
  }

  sortApplications(d) {
    console.log(d);
    return this.https.post(this.RecruitApp_Node + '/applications/apply', d)
      .pipe(map((response: any) => response));
  }

  getApplicationsReceived(d) {
    return this.https.get(this.RecruitApp_Node + '/applications/applications-received/' + d)
      .pipe(map((response: any) => response));
  }

  getApplicationsMade(d) {
    return this.https.get(this.RecruitApp_Node + '/applications/applications-made/' + d)
      .pipe(map((response: any) => response));
  }

  initCCAT(d) {
    console.log(d);
    return this.https.post(this.RecruitApp_Node + '/ccats/init-ccat', d)
      .pipe(map((response: any) => response));
  }

  getCCATCandidates() {
    return this.https.get(this.RecruitApp_Node + '/ccats/get-all-ccat-candidates')
      .pipe(map((response: any) => response));
  }
  
  getRecruiterCCAT(d) {
    return this.https.get(this.RecruitApp_Node + '/ccats/get-recruiter/' + d).pipe(map((response: any) => response));
  }

  getQuiz() {
    return this.https.get(this.RecruitApp_Node + '/quiz/get-questions')
      .pipe(map((response: any) => response));
  }

  wakeUpServer() {
    return this.https.get(this.RecruitApp_Node).pipe(map((response: any) => response));
  }


}
