import { TestBed } from '@angular/core/testing';

import { RecruitappgoService } from './recruitappgo.service';

describe('RecruitappgoService', () => {
  let service: RecruitappgoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecruitappgoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
