import {Component, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {MenuItems} from '../../_nav';

@Component({
  selector: 'app-custom-sidebar',
  template: `
        <div class="sidebar-content">
          <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                <ng-template ngFor let-navitem [ngForOf]="navigation">
                  <li *ngIf="isDivider(navitem)" class="navbar-divider"></li>
                  <ng-template [ngIf]="isTitle(navitem)">
                    <app-custom-sidebar-nav-title [title]='navitem'></app-custom-sidebar-nav-title>
                  </ng-template>
                  <ng-template [ngIf]="!isDivider(navitem)&&!isTitle(navitem)">
                    <app-custom-sidebar-nav-item [item]="navitem"></app-custom-sidebar-nav-item>
                  </ng-template>
                </ng-template>
            </ul>
          </div>
        </div>
  `
})
export class CustomSidebarComponent {
  public navigation = {};
  public isDivider(item) {
    return !!item.divider;
  }

  public isTitle(item) {
    return !!item.title;
  }

  constructor (public  nav: MenuItems) {
    this.nav.menu(sessionStorage.getItem('user')).subscribe(response => {
      this.navigation = response;
    });
  }

}
import { Router } from '@angular/router';
// import {AppSidebarNavComponent} from "../../../../../../../BaseApp-control/Base-App-Theme/src/app/components/app-sidebar-nav";

@Component({
  selector: 'app-custom-sidebar-nav-item',
  template: `<li *ngIf="!isDropdown(); else dropdown" [ngClass]="hasClass() ? 'nav-item ' + item.class : 'nav-item'">
                <app-custom-sidebar-nav-link [link]='item'></app-custom-sidebar-nav-link>
            </li>
        <ng-template #dropdown>
            <li [ngClass]="hasClass() ? 'nav-item nav-dropdown ' + item.class : 'nav-item nav-dropdown'"
                [class.open]="isActive()"
                routerLinkActive="open"
                appNavDropdown>
                <app-custom-sidebar-nav-dropdown [link]='item'></app-custom-sidebar-nav-dropdown>
            </li>
        </ng-template>
  `
})
export class CustomSidebarNavItemComponent {
  @Input() item: any;

  public hasClass() {
    return !!this.item.class;
  }

  public isDropdown() {
    return !!this.item.children;
  }

  public thisURL() {
    return this.item.url;
  }

  public isActive() {
    return this.router.isActive(this.thisURL(), false);
  }
  constructor (private router: Router) { }
}

@Component({
  selector: 'app-custom-sidebar-nav-link',
  template: `
    <a *ngIf="!isExternalLink(); else external"
      [ngClass]="hasVariant() ? 'nav-link nav-link-' + link.variant : 'nav-link'"
      routerLinkActive="active"
      [routerLink]="[link.url]">
      <i *ngIf="isIcon()" class="{{ link.icon }}"></i>
      {{ link.name }}
      <span *ngIf="isBadge()" [ngClass]="'badge badge-' + link.badge.variant">{{ link.badge.text }}</span>
    </a>
    <ng-template #external>
      <a [ngClass]="hasVariant() ? 'nav-link nav-link-' + link.variant : 'nav-link'" href="{{link.url}}">
        <i *ngIf="isIcon()" class="{{ link.icon }}"></i>
        {{ link.name }}
        <span *ngIf="isBadge()" [ngClass]="'badge badge-' + link.badge.variant">{{ link.badge.text }}</span>
      </a>
    </ng-template>
  `
})
export class CustomSidebarNavLinkComponent {
  @Input() link: any;

  public hasVariant() {
    return !!this.link.variant;
  }

  public isBadge() {
    return !!this.link.badge;
  }

  public isExternalLink() {
    return this.link.url.substring(0, 4) === 'http';
  }

  public isIcon() {
    return !!this.link.icon;
  }

  constructor() { }
}

@Component({
  selector: 'app-custom-sidebar-nav-dropdown',
  template: `
    <a class="nav-link nav-dropdown-toggle" appNavDropdownToggle>
      <i *ngIf="isIcon()" class="{{ link.icon }}"></i>
      {{ link.name }}
      <span *ngIf="isBadge()" [ngClass]="'badge badge-' + link.badge.variant">{{ link.badge.text }}</span>
    </a>
    <ul class="nav-dropdown-items">
      <ng-template ngFor let-child [ngForOf]="link.children">
        <app-custom-sidebar-nav-item [item]='child'></app-custom-sidebar-nav-item>
      </ng-template>
    </ul>
  `
})
export class CustomSidebarNavDropdownComponent {
  @Input() link: any;

  public isBadge() {
    return !!this.link.badge;
  }

  public isIcon() {
    return !!this.link.icon;
  }

  constructor() { }
}

@Component({
  selector: 'app-custom-sidebar-nav-title',
  template: ''
})
export class CustomSidebarNavTitleComponent implements OnInit {
  @Input() title: any;

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    const nativeElement: HTMLElement = this.el.nativeElement;
    const li = this.renderer.createElement('li');
    const name = this.renderer.createText(this.title.name);

    this.renderer.addClass(li, 'nav-title');

    if ( this.title.class ) {
      const classes = this.title.class;
      this.renderer.addClass(li, classes);
    }

    if ( this.title.wrapper ) {
      const wrapper = this.renderer.createElement(this.title.wrapper.element);

      this.renderer.appendChild(wrapper, name);
      this.renderer.appendChild(li, wrapper);
    } else {
      this.renderer.appendChild(li, name);
    }
    this.renderer.appendChild(nativeElement, li);
  }
}

export const APP_SIDEBAR_NAV = [
  CustomSidebarComponent,
  CustomSidebarNavDropdownComponent,
  CustomSidebarNavItemComponent,
  CustomSidebarNavLinkComponent,
  CustomSidebarNavTitleComponent
];
