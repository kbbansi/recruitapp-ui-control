// JobDescription will instantiate a job description
export class JobDescription {
    description: string;
    createdBy: Number;
    organization: string;
    keywords: KeyWord[];
    requirements: Requirement[];
}

// Requirement is now KeyPhase
export class Requirement {
    skill: string;
    weight: Number;
}

export class KeyWord {
    word: string;
    weight: Number;
}
