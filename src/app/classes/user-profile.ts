// UserProfile will instantiate a user profile for saving onto the
// document db
export class UserProfile {
    experience: Experiences[];
    skills: Skill[];
}

export class Experiences {
    previousWorkPlace: string;
    dateBegun: string;
    dateEnded: string;
    duties: string[];
}

export class Skill {
    tag: string;
    skillName: string;
}
