export interface CcatTest {
    question: string[];
    answers: string[];
    correctAnswer: string;
    answerWeight: Number;
}
