import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecruiterFeedComponent } from './recruiter-feed/recruiter-feed.component';
import { RecruiterProfileComponent } from './recruiter-profile/recruiter-profile.component';
import { RecruiterApplicationsComponent } from './recruiter-applications/recruiter-applications.component';
import { JobPostsComponent } from './job-posts/job-posts.component';
import { CcatTestComponent } from './ccat-test/ccat-test.component';

// set up recruiter-app routes

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Recruiter'
    },
    children: [
      {
        path: 'recruiter-feed', // all recruiter created job posts
        component: RecruiterFeedComponent,
        data: {
          title: 'Created Job Posts'
        }
      },
      {
        path: 'recruiter-profile', // recruiter profile
        component: RecruiterProfileComponent,
        data: {
          title: 'Profile'
        }
      },
      {
        path: 'recruiter-applications', // all job applications received
        component: RecruiterApplicationsComponent,
        data: {
          title: 'Job Applications'
        }
      },
      {
        path: 'job-posts', // job post manager
        component: JobPostsComponent,
        data: {
          title: 'Create Job Post'
        }
      },
      {
        path: 'ccat-test',
        component: CcatTestComponent,
        data: {
          title: 'Cognitive Criteria Aptitude Test'
        }
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule
  ]
})
export class RecruiterAppRoutingModule { }
