import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterFeedComponent } from './recruiter-feed.component';

describe('RecruiterFeedComponent', () => {
  let component: RecruiterFeedComponent;
  let fixture: ComponentFixture<RecruiterFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruiterFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
