import {INavData } from '@coreui/angular';

export const recruiterNav: INavData[] = [

  {
    name: 'Dashboard',
    url: 'recruiter/recruiter-feed',
    icon: 'icon-settings'
  },
  {
    name: 'Job Posts',
    url: 'recruiter/job-posts',
    icon: ''
  },
  {
    name: 'Applications',
    url: 'recruiter/recruiter-applications',
    icon: ''
  },
  {
    name: 'Profile',
    url: 'recruiter/recruiter-profile',
    icon: ''
  },
];
