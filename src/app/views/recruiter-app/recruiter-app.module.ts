import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecruiterApplicationsComponent } from './recruiter-applications/recruiter-applications.component';
import { RecruiterProfileComponent } from './recruiter-profile/recruiter-profile.component';
import { JobPostsComponent } from './job-posts/job-posts.component';
import { RecruiterFeedComponent } from './recruiter-feed/recruiter-feed.component';
import { RecruiterAppRoutingModule } from './recruiter-app-routing.module';
import { CcatTestComponent } from './ccat-test/ccat-test.component';
import { HttpClientModule } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [
    RecruiterApplicationsComponent,
    RecruiterProfileComponent,
    JobPostsComponent,
    RecruiterFeedComponent,
    CcatTestComponent,

  ],
  imports: [
    CommonModule,
    RecruiterAppRoutingModule,
    HttpClientModule,
    TabsModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BsDropdownModule,
    ModalModule.forRoot()
  ]
})
export class RecruiterAppModule { }
