import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcatTestComponent } from './ccat-test.component';

describe('CcatTestComponent', () => {
  let component: CcatTestComponent;
  let fixture: ComponentFixture<CcatTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcatTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcatTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
