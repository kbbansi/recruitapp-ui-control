import { Component, OnInit, TemplateRef } from '@angular/core';
import { CcatTest } from '../../../classes/ccat-test';
import { RecruitappgoService } from '../../../services/recruitappgo.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';



@Component({
  selector: 'app-ccat-test',
  templateUrl: './ccat-test.component.html',
  styleUrls: ['./ccat-test.component.css']
})
export class CcatTestComponent implements OnInit {
  modalRef: BsModalRef;
  quizFormGroup: FormGroup;
  quiz: any = {};


  constructor(private formBuilder: FormBuilder, private api: RecruitappgoService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getQuiz();
  }

  getQuiz() {
    this.quiz = {};
    this.api.getQuiz().subscribe(response => {
      console.log(response);
      if (response.status === 200) {
        this.quiz = response.message[0].quiz;
        console.log(this.quiz);
      } else {
        alert('No Tests at the moment');
      }
    });
  }

  viewAnswers(d, template: TemplateRef<any>) {
    console.log(d.answers);
    this.quizFormGroup = this.formBuilder.group({
      answers: [d.answers],
      correctAnswer: [d.answer],
      yourAnswer: ['', Validators.required],
      weight: [parseInt(d.weight, 10)],
      quizSubmit: this.formBuilder.array([this.formBuilder.group({question: d.question, answer: ''})])
    });
    console.log(this.quizFormGroup.value);
    this.modalRef = this.modalService.show(template);
  }

  calculate() {
    let score = 0;
    const totalScore = [];
    const yourAnswer = this.quizFormGroup.value['yourAnswer'];
    const correctAnswer = this.quizFormGroup.value['correctAnswer'];
    const weight = this.quizFormGroup.value['weight'];
    console.log(yourAnswer, correctAnswer, weight);

    if (yourAnswer === correctAnswer) {
      score += weight;
      totalScore.push(score);
    }

    this.addUserQuiz();
  }

  get userQuiz() {
    return this.quizFormGroup.get('quizSubmit') as FormArray;
  }

  addUserQuiz() {
    this.userQuiz.push(this.formBuilder.group({question: '', answer: ''}));
  }

}
