import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFeedComponent } from './user-feed/user-feed.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserApplicationsComponent } from './user-applications/user-applications.component';
import { UserAppRoutingModule } from './user-app-routing.module';



@NgModule({
  declarations: [UserFeedComponent, UserProfileComponent, UserApplicationsComponent],
  imports: [
    CommonModule,
    UserAppRoutingModule
  ]
})
export class UserAppModule { }
