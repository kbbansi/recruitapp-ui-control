import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { Routes, RouterModule } from '@angular/router';
import { UserFeedComponent } from './user-feed/user-feed.component';
import { UserApplicationsComponent } from './user-applications/user-applications.component';

// set up user-app routes

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'User'
    },
    children: [
      {
        path: 'user-feed', // job post feed
        component: UserFeedComponent,
        data: {
          title: 'Feed'
        }
      },
      {
        path: 'user-profile', // profile set up
        component: UserProfileComponent,
        data: {
          title: 'User Profile'
        }
      },
      {
        path: 'user-applications', // applications made
        component: UserApplicationsComponent,
        data: {
          title: 'Applications'
        }
      }
    ]
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class UserAppRoutingModule { }
