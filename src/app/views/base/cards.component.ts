import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RecruitappgoService } from '../../services/recruitappgo.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
@Component({
  templateUrl: 'cards.component.html'
})
export class CardsComponent implements OnInit {
  JobPostForm: FormGroup;
  jsonData: any;
  modalRef: BsModalRef;
  JobDescription: FormGroup;
  goJson: any = {}; // for storing the documentID from the mongo server

  userID: any;
  firstName: any;
  lastName: any;
  otherNames: any;
  email: any;
  phone: any;
  userType: any;
  created_by: any;
  title: any;
  organization: any;

  constructor(private formBuilder: FormBuilder, private api: RecruitappgoService, private modalService: BsModalService) {
    this.userID = sessionStorage.getItem('id');
    this.phone = sessionStorage.getItem('contact');
    this.firstName = sessionStorage.getItem('first_name');
    this.lastName = sessionStorage.getItem('last_name');
    this.otherNames = sessionStorage.getItem('other_names');
    this.email = sessionStorage.getItem('email');

    this.createJobPostForm();
  }

  ngOnInit() {
    this.created_by = parseInt(this.userID, 10);
    this.getAllUserPosts();

    this.JobDescription = this.formBuilder.group({
      title: ['', Validators.required],
      organization: ['', Validators.required],
      createdBy: [this.created_by],
      description: ['', Validators.required],
      requirements: this.formBuilder.array([this.formBuilder.group({ skill: '', weight: '' })]),
      keyphrases: this.formBuilder.array([this.formBuilder.group({ word: '', weight: '' })])
    });
  }

  createJobPostForm() {
    this.JobPostForm = this.formBuilder.group({
      title: ['', Validators.required],
      organization: ['', Validators.required],
      created_by: [this.created_by]
    });
  }

  saveJobPost() {
    console.log(this.JobPostForm.value);
    this.jsonData = {};
    this.api.createJobPost(this.JobPostForm.value).subscribe(response => {
      console.log(response);
      if (response.status === 201) {
        this.jsonData = response.data;
        alert('Job Post Successfully Saved');
        console.log(this.jsonData.actionType);
        this.JobPostForm.reset();
        this.ngOnInit();
        this.modalRef.hide();
      } else {
        alert('An Error Occurred');
        console.log(this.jsonData.actionType);
        this.JobPostForm.reset();
        this.ngOnInit();
        this.modalRef.hide();
      }
    });
  }

  openJobPostModal(template: TemplateRef<any>) {
    this.JobPostForm = this.formBuilder.group({
      title: ['', Validators.required],
      organization: ['', Validators.required],
      created_by: [this.created_by]
    });
    this.modalRef = this.modalService.show(template);
  }

  getAllUserPosts() {
    console.log(this.created_by);
    this.jsonData = {};
    this.api.goGetUserCreatedJobPosts(this.created_by).subscribe(response => {
      console.log(response);
      if (response.status === 404) {
        console.log('No Job Posts here');
        alert('No Job Posts Yet');
      } else {
        this.jsonData = response.data;
        console.log(this.jsonData);
      }
    });
  }

  // getInfo will get the job title and organization for a job post
  getInfo(data: any) {
    console.log(data);
    this.title = data.title;
    this.organization = data.organization;

    this.JobDescription.patchValue({
      title: this.title,
      organization: this.organization
    });
  }
  // job description functionality starts here


  // 1. get job requirements as form arrary
  get jobRequirements() {
    return this.JobDescription.get('requirements') as FormArray;
  }

  // 2. get job key phrases as form array
  get jobKeyPhrase() {
    return this.JobDescription.get('keyphrases') as FormArray;
  }

  // 3. add a job requirement
  addJobRequirement() {
    this.jobRequirements.push(this.formBuilder.group({ skill: '', weight: '' }));
  }

  // 4. add a key phrase
  addJobKeyPhrase() {
    this.jobKeyPhrase.push(this.formBuilder.group({ word: '', weight: '' }));
  }

  saveJobDescription() {
    console.log(this.JobDescription.value);
    this.api.saveJobDescription(this.JobDescription.value).subscribe(response => {
      console.log(response);
      if (response.status !== 201) {
        alert('Could not create Job Description at this time');
        console.log(response.message);
        this.JobDescription.reset();
        this.ngOnInit();
      } else {
        alert('Job Description Created');
        console.log(response.message);
      }
    });

  }
}
