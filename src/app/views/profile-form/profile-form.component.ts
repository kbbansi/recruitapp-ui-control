import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { RecruitappgoService } from '../../services/recruitappgo.service';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css']
})
export class ProfileFormComponent implements OnInit {
  profileForm: FormGroup;
  userID: any;
  goJson: any = {};

  constructor(private formBuilder: FormBuilder, private api: RecruitappgoService) {
    this.userID = sessionStorage.getItem('id');
  }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      bio: [],
      currentWorkPlace: ['', Validators.required],
      currentRole: ['', Validators.required],
      userID: [parseInt(this.userID, 10)],
      experience: this.formBuilder.array([this.formBuilder.group({previousWorkPlace: '', dateBegun: '', dateEnded: ''})]),
      skills: this.formBuilder.array([this.formBuilder.group({tag: '', skillName: ''})]),
      duties: this.formBuilder.array([this.formBuilder.group({task: ''})])
    });
  }

  // get experience
  get userExperience() {
    return this.profileForm.get('experience') as FormArray;
  }

  get userSkill() {
    return this.profileForm.get('skills') as FormArray;
  }

  // add an experience
  addUserExperience() {
    // this.userExperience.push(this.formBuilder.group({experience: ''}));
    this.userExperience.push(this.formBuilder.group({duties: '', previousWorkPlace: '', dateBegun: '', dateEnded: ''}));
  }

  // add user skill
  addUserSkill () {
    this.userSkill.push(this.formBuilder.group({tag: '', skillName: ''}));
  }

  get userDuties() {
    return this.profileForm.get('duties') as FormArray;
  }

  addUserDuty() {
    this.userDuties.push(this.formBuilder.group({task: ''}));
  }

  deleteUserExperience(index) {
    this.userExperience.removeAt(index);
  }

  deleteUserSkill(index) {
    this.userSkill.removeAt(index);
  }

  save() {
    // console.log(this.profileForm.value);
    this.goJson = {};
    this.api.createUserProfile(this.profileForm.value).subscribe(response => {
      console.log(response);
      if (response.status !== 200 ) {
        alert('An Error Occurred');
        console.log(response.message);
      } else {
        alert('Success');
        console.log(response.message);
        this.ngOnInit();
        // make another request to go server
      }
    });
  }
}
