import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RecruitappgoService } from '../../services/recruitappgo.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  RegistrationForm: FormGroup;
  stream: any = {};
  user_type = 'recruiter';

  constructor(private formBuilder: FormBuilder, private router: Router, private api: RecruitappgoService) {
    this.createRegistrationForm();
  }

  ngOnInit() {
    this.wakeUpServer();
  }

  createRegistrationForm() {
    this.RegistrationForm = this.formBuilder.group({
      contact: ['', Validators.required],
      email: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      password: ['', Validators.required],
      user_type: [this.user_type]
    });
  }

  register() {
    console.log(this.RegistrationForm.value);
    this.api.register(this.RegistrationForm.value).subscribe(response => {
      this.stream = response;
      // todo:: check response status
      console.log(response);
      if (response.status === 400){
        alert('An Error occurred');
        console.log(response.status)
      } else {
        this.router.navigate(['login']).then(r => console.log(r));
      }
    });
  }

  wakeUpServer() {
    this.api.wakeUpServer().subscribe(response => {
      console.log(response);
    })
  }
}
