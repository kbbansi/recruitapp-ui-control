import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RecruitappgoService } from '../../services/recruitappgo.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  jobLists: any = {};
  jobDescriptions: any = {};
  JobPostListFormGroup: FormGroup;
  modalRef: BsModalRef;

  // session variables
  userID: any;
  firstName: any;
  lastName: any;
  otherNames: any;
  email: any;
  phone: any;
  userType: any;
  userDocumentID: any;
  iterable: [];

  constructor(private api: RecruitappgoService, private formBuilder: FormBuilder, private modalService: BsModalService) {
    this.userID = sessionStorage.getItem('id');
    this.phone = sessionStorage.getItem('contact');
    this.firstName = sessionStorage.getItem('first_name');
    this.lastName = sessionStorage.getItem('last_name');
    this.otherNames = sessionStorage.getItem('other_names');
    this.email = sessionStorage.getItem('email');
    this.userDocumentID = sessionStorage.getItem('user_document_id');
    //
  }
  ngOnInit(): void {
     // this.getAllJobPosts();
    this.getAllJobPostDescriptions();
  }

  // call to go server will be replaced with call to mongo server
  // getAllJobPosts() {
  //   console.log('Jobs');
  //   this.jobLists = {};
  //   this.api.getAllJobPosts().subscribe(response => {
  //     console.log(response);
  //     if (response.status === 200) {
  //       this.jobLists = response.data;
  //       console.log(this.jobLists);
  //     } else {
  //       console.log('No Data');
  //     }
  //   });
  // }

  getAllJobPostDescriptions() {
    console.log('Descriptions');
    this.jobDescriptions = {};
    let createdBy = parseInt(this.userID, 10);
    this.api.getUserCreatedJobPosts(createdBy).subscribe(response => {
      console.log(response);
      if (response.status === 200) {
        this.jobDescriptions = response.message;
        console.log(this.jobDescriptions);
      } else {
        console.log('Nawa oo');
      }
    });
  }

  goGetAllJobPosts() {
    this.jobDescriptions = {};
    let id = parseInt(this.userID, 10);
    this.api.goGetUserCreatedJobPosts(id).subscribe(response => {
      console.log(response);
      if (response.status === 200) {
        this.jobDescriptions = response.message;
        console.log(this.jobDescriptions);
      } else {
        console.log('Nawa oo');
      }
    });
  }

  viewJobDetails(d, template: TemplateRef<any>) {
    console.log(d.description);
    this.JobPostListFormGroup = this.formBuilder.group({
      title: [d.title],
      organization: [d.organization],
      description: [d.description],
      createdBy: [d.createdBy],
      userID: [parseInt(this.userID, 10)],
      postID: [d.postID],
      job_post_document_id: [d._id]
    });
    this.modalRef = this.modalService.show(template);

    console.log(this.JobPostListFormGroup.value);
  }

  startApplication() {
    console.log(this.JobPostListFormGroup.value);
    // startApplication will create the application on the MySQL database and then
    // reroute the user to the Applications view to Finish the Application Process
    this.api.initApplication(this.JobPostListFormGroup.value).subscribe(response => {
      console.log(response);
      if (response.status === 201) {
        this.sortApplication();
      } else {
        alert('Application Process Could Not Be Started at This Time');
        console.log(response.fail);
      }
    });
  }

  sortApplication() {
    console.log(this.JobPostListFormGroup.value);
    this.api.sortApplications(this.JobPostListFormGroup.value).subscribe(response => {
      console.log(response);
      if (response.status === 201) {
        console.log('Application Sorted');
        alert('Application Sorted');
        console.log(response.applicationStream);
      } else {
        console.log('An Error Occured');
        alert('Application Cannot Be Sent at this Time');
      }
    });
  }
}
