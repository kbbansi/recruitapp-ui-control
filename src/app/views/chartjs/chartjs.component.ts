import { Component, OnInit, TemplateRef } from '@angular/core';
import { RecruitappgoService } from '../../services/recruitappgo.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'chartjs.component.html'
})
export class ChartJSComponent implements OnInit {
  // session variables
  userID: any;
  firstName: any;
  lastName: any;
  otherNames: any;
  email: any;
  phone: any;
  userType: any;
  userDocumentID: any;
  userInformation: FormGroup;
  userProfile: FormGroup;
  experiences: FormGroup;
  skills: FormGroup;
  duties: FormGroup;
  modalRef: BsModalRef;

  constructor(private api: RecruitappgoService, private formBuilder: FormBuilder, private modalService: BsModalService) {
    this.userID = sessionStorage.getItem('id');
    this.phone = sessionStorage.getItem('contact');
    this.firstName = sessionStorage.getItem('first_name');
    this.lastName = sessionStorage.getItem('last_name');
    this.otherNames = sessionStorage.getItem('other_names');
    this.email = sessionStorage.getItem('email');
    this.userDocumentID = sessionStorage.getItem('user_document_id');
    //
    this.createUserInformationForm();
    this.createUserProfileForm();
    this.createDutiesForm();
    this.createSkillsForm();
    this.createExperienceForm();
  }
  ngOnInit(): void { }

  createUserInformationForm() {
    this.userInformation = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      otherNames: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
    });
  }

  createUserProfileForm() {
    this.userProfile = this.formBuilder.group({
      userID: [this.userID],
      bio: ['', Validators.required],
      currentWorkPlace: ['', Validators.required],
      skills: ['', Validators.required],
      experience: ['', Validators.required]
    });
  }

  createSkillsForm() {
    this.skills = this.formBuilder.group({
      tag: ['', Validators.required],
      skillName: ['', Validators.required],
    });
  }

  createExperienceForm() {
    this.experiences = this.formBuilder.group({
      previousWorkPlace: ['', Validators.required],
      dateBegun: ['', Validators.required],
      dateEnded: ['', Validators.required],
      duties: ['', Validators.required],
    });
  }

  createDutiesForm() {
    this.duties = this.formBuilder.group({
      task: ['', Validators.required],
      organization: ['', Validators.required],
    });
  }

  saveProfile() {
    this.experiences.patchValue({
      duties: this.duties.value
    });
    this.userProfile.patchValue({
      experience: this.experiences.value,
      skills: this.skills.value
    });

    console.log(this.userProfile.value);
  }

  // open up form modals

  // openExperiencesModal will open the experiences modal to add an experiences
  openExperiencesModal(template: TemplateRef<any>) {
    this.experiences = this.formBuilder.group({
      previousWorkPlace: ['', Validators.required],
      dateBegun: ['', Validators.required],
      dateEnded: ['', Validators.required],
      duties: ['', Validators.required],
    });
    this.modalRef = this.modalService.show(template);
  }
}
