import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { ChartJSComponent } from './chartjs.component';
import { ChartJSRoutingModule } from './chartjs-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';
import { ProfileFormComponent } from '../profile-form/profile-form.component';

@NgModule({
  imports: [
    ChartJSRoutingModule,
    ChartsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    CommonModule
  ],
  declarations: [ ChartJSComponent, ProfileFormComponent ],
})
export class ChartJSModule { }
