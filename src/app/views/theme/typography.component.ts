import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RecruitappgoService } from '../../services/recruitappgo.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'typography.component.html'
})
export class TypographyComponent implements OnInit {

  userID: any;
  firstName: any;
  lastName: any;
  otherNames: any;
  email: any;
  phone: any;
  userType: any;

  CCATFormGroup: FormGroup;
  aboutCCAT: string;
  ccatCandidates: any = {};
  modalRef: BsModalRef;
  constructor(private api: RecruitappgoService, private formBuilder: FormBuilder, private modalService: BsModalService) {
    this.userID = sessionStorage.getItem('id');
    this.phone = sessionStorage.getItem('contact');
    this.firstName = sessionStorage.getItem('first_name');
    this.lastName = sessionStorage.getItem('last_name');
    this.otherNames = sessionStorage.getItem('other_names');
    this.email = sessionStorage.getItem('email');


  }

  ngOnInit(): void {
    this.aboutCCAT = `This test is a pre-employment aptitude test that measures an individual’s ability to solve problems, digest and apply information, learn new skills and think critically. Individuals with high scores on this test are potential high performers on the given job. The test comprises 50 items(questions) to be attempted within a 15-minute time limit.`;

    this.getCCATCandidates();
  }

  setCCATParams(d, template: TemplateRef<any>) {
    console.log(this.aboutCCAT);
    this.CCATFormGroup = this.formBuilder.group({
      aboutCCAT: [this.aboutCCAT],
      _id: [d._id],
      testStatus: ['Active'],
      userID: [d.userID],
      userStatus: [d.userEligibility],
      createdBy: [parseInt(this.userID, 10)],
      applicantName: [d.applicantName]
    });
    this.modalRef = this.modalService.show(template);
  }

  getCCATCandidates() {
    this.ccatCandidates = {};
    let createdBy = parseInt(this.userID, 10);
    this.api.getRecruiterCCAT(createdBy).subscribe(response => {
      console.log(response);
      if (response.status === 404) {
        alert('No Eligible CCAT Candidates');
      } else {
        this.ccatCandidates = response.message;
      }
    });
  }

  createCCAT() {
    console.log(this.CCATFormGroup.value);
    this.api.initCCAT(this.CCATFormGroup.value).subscribe(response => {
      console.log(response);
      if (response.status === 400) {
        alert('Cannot Initiate CCAT for user at this time');
        this.modalRef.hide();
        this.ngOnInit();
      } else {
        alert('Initiated CCAT');
        this.modalRef.hide();
        this.ngOnInit();
      }
    });

  }
}
