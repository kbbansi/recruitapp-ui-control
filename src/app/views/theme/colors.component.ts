import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RecruitappgoService } from '../../services/recruitappgo.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'colors.component.html'
})
export class ColorsComponent implements OnInit {
  // session variables
  userID: any;
  firstName: any;
  lastName: any;
  otherNames: any;
  email: any;
  phone: any;
  userType: any;

  ApplicationDetailsFormGroup: FormGroup;
  details: 0;
  applicationList: any = {};
  receivedApplications: any = {};
  modalRef: BsModalRef;

  constructor(private api: RecruitappgoService, private formBuilder: FormBuilder, private modalService: BsModalService) {
    this.userID = sessionStorage.getItem('id');
    this.phone = sessionStorage.getItem('contact');
    this.firstName = sessionStorage.getItem('first_name');
    this.lastName = sessionStorage.getItem('last_name');
    this.otherNames = sessionStorage.getItem('other_names');
    this.email = sessionStorage.getItem('email');
  }

  ngOnInit(): void {
    this.getApplicationsMade();
    this.getApplicationsReceived();
  }

  getApplicationsMade() {
    const userID = parseInt(this.userID, 10);
    this.applicationList = {};
    this.api.getApplicationsMade(userID).subscribe(response => {
      console.log(response);
      if (response.status === 404) {
        alert('No Data');
      } else {
        //
        this.applicationList = response.message;
      }
    });

  }

  getApplicationsReceived() {
    const userID = parseInt(this.userID, 10);
    this.receivedApplications = {};
    this.api.getApplicationsReceived(userID).subscribe(response => {
      console.log(response);
      if (response.status === 404) {
        alert(response.status);
      } else {
        //
        this.receivedApplications = response.message;
      }
    });

  }

  showApplicationDetails(d, template: TemplateRef<any>) {
    console.log(d);
    this.ApplicationDetailsFormGroup = this.formBuilder.group({
      jobTitle: [d.jobTitle],
      totalUserScore: [d.totalUserScore],
      userEligibility: [d.userEligibility],
      createdBy: [d.postOwner],
      userID: [d.userID],
      applicationID: [d._id],
      applicantName: [d.applicantName],
      applicantEmail: [d.applicantEmail],
    });
    this.modalRef = this.modalService.show(template);
  }

  startTest() {
    // alert('Enter Testing Unit');
    console.log(this.ApplicationDetailsFormGroup.value);

    // init ccat
    this.api.initCCAT(this.ApplicationDetailsFormGroup.value).subscribe(response => {
      console.log(response);
      if (response.status === 201) {
        alert('Cognitive Criteria Aptitude Test Initialized');
        console.log(response.ccatStream);
        this.modalRef.hide();
        this.ngOnInit();
      } else {
        alert('Cannot initialize Cognitive Criteria Aptitude Test at this time');
        console.log(response.message);
        this.modalRef.hide();
        this.ngOnInit();
      }
    });
  }
}


/*
averageUserScore: 135
jobTitle: "Vacancy for Java Developers"
postID: 1
postOwner: 1
totalUserScore: 270
userEligibility: "Eligible"
userID: 2
__v: 0
_id: "5f05a67c553bab2a4883e65d"
*/
