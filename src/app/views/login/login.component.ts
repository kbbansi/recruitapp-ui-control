import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RecruitappgoService } from '../../services/recruitappgo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  stream: any;
  userType: any;

  constructor(private formBuilder: FormBuilder, private api: RecruitappgoService, private router: Router) {
    this.createLoginForm();
  }

  ngOnInit() {
    this.wakeUpServer();
  }

  createLoginForm() {
    this.LoginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  
  wakeUpServer() {
    this.api.wakeUpServer().subscribe(response => {
      console.log(response);
    })
  }

  login() {
    this.stream = {};
    console.log(this.LoginForm.value);
    this.api.login(this.LoginForm.value);
    this.api.login(this.LoginForm.value).subscribe(response => {
      console.log(response.session_data);
      this.stream = response;

      if (this.stream.status === 400) {
        console.log(this.stream.Reason);
        alert('An Error occurred');
      } else {
        console.log('success');
        console.log(this.stream.session_data);
        console.log(this.stream.SessionData);
        this.setSessionData(this.stream.session_data);

        // set up views per user_type
        // get user_type from session values
        // recruit_app_go_password: recruit app go db pass
        this.userType = sessionStorage.getItem('user_type');
        console.log(this.userType);
        this.router.navigate(['dashboard']);
      }
    });
  }

  setSessionData(d) {
    //
    sessionStorage.setItem('id', d.id);
    sessionStorage.setItem('contact', d.contact);
    sessionStorage.setItem('created_on', d.created_on);
    sessionStorage.setItem('email', d.email);
    sessionStorage.setItem('first_name', d.first_name);
    sessionStorage.setItem('last_name', d.last_name);
    sessionStorage.setItem('user_document_id', d.user_document_id);
    sessionStorage.setItem('user_type', d.user_type);
    sessionStorage.setItem('other_names', d.other_names);
  }
 }
/*
session data
contact: "0559633956"
created_on: ""
email: "jigbaeboo@gmail.com"
first_name: "Knaomi Amponfi"
id: 1
last_name: "Ampofo"
modified_on: ""
other_names: ""
password: ""
user_document_id: ""
user_type: "user"
,*/
