import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: 'buttons.component.html'
})
export class ButtonsComponent implements OnInit {
  date: any;
  constructor() { }

  ngOnInit() {
    this.date = new Date();
  }
}
