import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RecruitappgoService } from '../../services/recruitappgo.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.css']
})
export class JobDescriptionComponent implements OnInit {
  JobDescription: FormGroup;
  goJson: any = {}; // for storing the documentID from the mongo server
  userID: any;
  firstName: any;
  lastName: any;
  otherNames: any;
  email: any;
  phone: any;
  userType: any;
  created_by: any;
  constructor(private formBuilder: FormBuilder, private api: RecruitappgoService, private modalService: BsModalService) {
    this.userID = sessionStorage.getItem('id');
    this.phone = sessionStorage.getItem('contact');
    this.firstName = sessionStorage.getItem('first_name');
    this.lastName = sessionStorage.getItem('last_name');
    this.otherNames = sessionStorage.getItem('other_names');
    this.email = sessionStorage.getItem('email');

    // this.createJobPostForm();
  }


  ngOnInit(): void {
    this.created_by = parseInt(this.userID, 10);
    this.JobDescription = this.formBuilder.group({
      title: ['', Validators.required],
      organization: ['', Validators.required],
      createdBy: [this.created_by],
      description: ['', Validators.required],
      requirements: this.formBuilder.array([this.formBuilder.group({skill: '', weight: ''})]),
      keyphrases: this.formBuilder.array([this.formBuilder.group({word: '', weight: ''})])
    });
  }

  // 1. get job requirements as form arrary
  get jobRequirements() {
    return this.JobDescription.get('requirements') as FormArray;
  }

  // 2. get job key phrases as form array
  get jobKeyPhrase() {
    return this.JobDescription.get('keyphrases') as FormArray;
  }

  // 3. add a job requirement
  addJobRequirement() {
    this.jobRequirements.push(this.formBuilder.group({skill: '', weight: ''}));
  }

  // 4. add a key phrase
  addJobKeyPhrase() {
    this.jobKeyPhrase.push(this.formBuilder.group({word: '', weight: ''}));
  }

  saveJobDescription() {
    console.log(this.JobDescription.value);
  }
}
