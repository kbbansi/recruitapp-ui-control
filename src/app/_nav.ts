import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { RecruitappgoService } from './services/recruitappgo.service';
import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Feed', // dashboard view show activity summaries
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Desk Unit'
  },
  // app views
  {
    name: 'Applications', // Applications
    url: '/theme/colors',
    icon: 'icon-drop'
  },
  {
    name: 'Test Unit', // Psychometric Tests
    url: '/theme/typography',
    icon: 'icon-pencil'
  },
  {
    title: true,
    name: 'Offers'
  },
  {
    name: 'Job Posts', // Job Posts
    url: '/job-post',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Create a Job Post',
        url: '/job-post/create-job-post',
        icon: 'icon-puzzle'
      },
      // {
      //   name: 'View all Posts',
      //   url: '/job-post/view-all-posts',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Manage Job Post',
      //   url: '/job-post/manage-job-post',
      //   icon: 'icon-puzzle'
      // },
      {
        divider: true
      }
      // {
      //   title: true,
      //   name: 'Guides',
      // },
      // {
      //   name: 'Forms',
      //   url: '/job-post/forms',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Navbars',
      //   url: '/job-post/navbars',
      //   icon: 'icon-puzzle'

      // },
      // {
      //   name: 'Pagination',
      //   url: '/job-post/paginations',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Popovers',
      //   url: '/job-post/popovers',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Progress',
      //   url: '/job-post/progress',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Switches',
      //   url: '/job-post/switches',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Tables',
      //   url: '/job-post/tables',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Tabs',
      //   url: '/job-post/tabs',
      //   icon: 'icon-puzzle'
      // },
      // {
      //   name: 'Tooltips',
      //   url: '/job-post/tooltips',
      //   icon: 'icon-puzzle'
      // }
    ]
  },
  {
    name: 'Interview', // Interview Setup
    url: '/buttons',
    icon: 'icon-cursor',
    children: [
      {
        name: 'Create Interview', // Create Interview
        url: '/buttons/buttons',
        icon: 'icon-cursor'
      }
      // {
      //   name: 'Bias Check', // Check for Bias
      //   url: '/buttons/dropdowns',
      //   icon: 'icon-cursor'
      // },
      // {
      //   name: 'Past Interviews', // view past interviews
      //   url: '/buttons/brand-buttons',
      //   icon: 'icon-cursor'
      // }
    ]
  },
  {
    name: 'Your Profile', // User Profile
    url: '/profile',
    icon: 'icon-pie-chart'
  }
  // {
  //   name: 'Notifications', // Notifications
  //   url: '/icons',
  //   icon: 'icon-star',
  //   children: [
  //     {
  //       name: 'CoreUI Icons',
  //       url: '/icons/coreui-icons',
  //       icon: 'icon-star',
  //       badge: {
  //         variant: 'success',
  //         text: 'NEW'
  //       }
  //     },
  //     {
  //       name: 'Flags',
  //       url: '/icons/flags',
  //       icon: 'icon-star'
  //     },
  //     {
  //       name: 'Font Awesome',
  //       url: '/icons/font-awesome',
  //       icon: 'icon-star',
  //       badge: {
  //         variant: 'secondary',
  //         text: '4.7'
  //       }
  //     },
  //     {
  //       name: 'Simple Line Icons',
  //       url: '/icons/simple-line-icons',
  //       icon: 'icon-star'
  //     }
  //   ]
  // },
  // {
  //   divider: true
  // },
  // {
  //   title: true,
  //   name: 'Guides',
  // },
  // {
  //   name: 'Notifications',
  //   url: '/notifications',
  //   icon: 'icon-bell',
  //   children: [
  //     {
  //       name: 'Alerts',
  //       url: '/notifications/alerts',
  //       icon: 'icon-bell'
  //     },
  //     {
  //       name: 'Badges',
  //       url: '/notifications/badges',
  //       icon: 'icon-bell'
  //     },
  //     {
  //       name: 'Modals',
  //       url: '/notifications/modals',
  //       icon: 'icon-bell'
  //     }
  //   ]
  // },
  // {
  //   name: 'Widgets',
  //   url: '/widgets',
  //   icon: 'icon-calculator',
  //   badge: {
  //     variant: 'info',
  //     text: 'NEW'
  //   }
  // },
  // {
  //   divider: true
  // },
  // {
  //   title: true,
  //   name: 'Extras',
  // },
  // {
  //   name: 'Pages',
  //   url: '/pages',
  //   icon: 'icon-star',
  //   children: [
  //     {
  //       name: 'Login',
  //       url: '/login',
  //       icon: 'icon-star'
  //     },
  //     {
  //       name: 'Register',
  //       url: '/register',
  //       icon: 'icon-star'
  //     },
  //     {
  //       name: 'Error 404',
  //       url: '/404',
  //       icon: 'icon-star'
  //     },
  //     {
  //       name: 'Error 500',
  //       url: '/500',
  //       icon: 'icon-star'
  //     }
  //   ]
  // },
  // {
  //   name: 'Disabled',
  //   url: '/dashboard',
  //   icon: 'icon-ban',
  //   badge: {
  //     variant: 'secondary',
  //     text: 'NEW'
  //   },
  //   attributes: { disabled: true },
  // },
  // {
  //   name: 'Download CoreUI',
  //   url: 'http://coreui.io/angular/',
  //   icon: 'icon-cloud-download',
  //   class: 'mt-auto',
  //   variant: 'success',
  //   attributes: { target: '_blank', rel: 'noopener' }
  // },
  // {
  //   name: 'Try CoreUI PRO',
  //   url: 'http://coreui.io/pro/angular/',
  //   icon: 'icon-layers',
  //   variant: 'danger',
  //   attributes: { target: '_blank', rel: 'noopener' }
  // }
];


// @Injectable()
// export class MenuItems {
//   selection;
//   SimpleObserver = new Observable((observer) => {
//     switch (this.selection) {
//       case 'user':
//         observer.next([
//           {
//             name: 'Feed',
//             url: 'user/user-feed',
//             icon: ''
//           },
//           {
//             name: 'Applications',
//             url: 'user/user-applications',
//             icon: ''
//           },
//           {
//             name: 'User Profile',
//             url: 'user/user-profile',
//             icon: 'icon-settings'
//           }
//         ]);
//         break;

//         case 'recruiter':
//           observer.next([
//             {
//               name: 'Dashboard',
//               url: 'recruiter/recruiter-feed',
//               icon: 'icon-settings'
//             },
//             {
//               name: 'Job Posts',
//               url: 'recruiter/job-posts',
//               icon: ''
//             },
//             {
//               name: 'Applications',
//               url: 'recruiter/recruiter-applications',
//               icon: ''
//             },
//             {
//               name: 'Profile',
//               url: 'recruiter/recruiter-profile',
//               icon: ''
//             },
//           ]);
//           break;

//           default:
//             alert('You don\'t have the necessary permission to access this application. Contact support at support@recruitapp.com');
//             this.router.navigate(['register']);
//             break;
//     }
//     observer.complete();
//   });

//   constructor(private api: RecruitappgoService, private router: Router) {
//     const user = sessionStorage.getItem('user_type');
//     this.api.getUserMenuType(user).subscribe(response => {
//       console.log(response);
//       this.selection = response;
//     });
//   }

//   menu(menuList) {
//     this.selection = menuList;
//     return this.SimpleObserver;
//   }
// }
