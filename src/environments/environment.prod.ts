export const environment = {
  production: true,
  go_url: 'https://recruit-app-mongo.herokuapp.com',
  node_mongo_url: 'https://recruit-app-mongo.herokuapp.com'
};
